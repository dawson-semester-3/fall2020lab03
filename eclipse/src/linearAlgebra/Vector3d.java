// Neil Fisher (1939668)

package linearAlgebra;

public class Vector3d {
	private double x, y, z;

	public Vector3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public static void main(String[] args) {
		Vector3d vector = new Vector3d(1, 2, 3);
		System.out.println(vector);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	public double magnitude() {
		return Math.sqrt(Math.pow(getX(), 2) + Math.pow(getY(), 2) + Math.pow(getZ(), 2));
	}

	public double dotProduct(Vector3d dot) {
		return (this.getX() * dot.getX()) + (this.getY() * dot.getY()) + (this.getZ() * dot.getZ());
	}

	public Vector3d add(Vector3d vector2) {
		// Vector3d newVector = new Vector3d(newX, newY, newZ);
		return new Vector3d(getX() + vector2.getX(), getY() + vector2.getY(), getZ() + vector2.getZ());
	}
}