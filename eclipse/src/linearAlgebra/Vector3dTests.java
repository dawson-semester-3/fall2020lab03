package linearAlgebra;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class Vector3dTests {
	Vector3d vector = new Vector3d(1, 2, 3);
	Vector3d vector2 = new Vector3d(4, 5, 6);

	@Test
	void testGetMethods() {
		assertEquals(1, vector.getX());
		assertEquals(2, vector.getY());
		assertEquals(3, vector.getZ());
	}

	@Test
	void testMagnitude() {
		assertEquals(Math.sqrt(14), vector.magnitude());
	}

	@Test
	void testDotProduct() {
		assertEquals(32, vector.dotProduct(vector2));
	}

	@Test
	void testAdding() {
		Vector3d vectorReturned = vector.add(vector2);
		assertEquals(5, vectorReturned.getX());
		assertEquals(7, vectorReturned.getY());
		assertEquals(9, vectorReturned.getZ());
	}

}
